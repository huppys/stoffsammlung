# Welche Bildungsprojekte gibt es bereits? #

## Initiativen des Bundes
* [Initiative Digitale Bildung](https://www.bmbf.de/de/bildung-digital-3406.html)
	* [KI-Campus](https://www.bmbf.de/de/media-video-47887.html)
	* vhs-Bildungsportal
		* [Material und Anleitungen](https://www.vhs-lernportal.de/wws/material.php)
		* [Mein neues Handy](https://grundbildung.vhs-lernportal.de/wws/9.php#/wws/medienkompetenz.php)
	* [Wir lernen online](https://wirlernenonline.de/)
	

### Kompetenz: Data Literacy
* [Stadt.Land.Datenfluss. Die App für mehr Datenkompetenz](https://www.stadt-land-datenfluss.de/)

## NGOs
* [UNESCO - Bildung für nachhaltige Entwicklung](https://www.unesco.de/bildung/bne-akteure)
* Open Knowledge Foundation Deutschland e.V. (OKF)
* mediale pfade – Verein für Medienbildung e.V.
* Open Education Resource (OER)
* [Jugend hackt](https://jugendhackt.org/)

