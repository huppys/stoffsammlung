# Stoffsammlung #

Jeder große Gedanke fängt mal klein an.

### Warum gibt es diese Stoffsammlung? ###

* Überblick der Themenfelder rund um Bildung zum Umgang mit digitalen Medien.

### Was beinhaltet diese Stoffsammlung? ###

* Alles, was es über digitale Medien zu wissen gibt.
* Wir fangen in der Breite an und arbeiten uns dann in die Tiefe vor.

### An wen richtet sich die Stoffsammlung? ###

* Zunächst an alle, die Anregungen suchen, um sich mit digitalen Medien auseinander zu setzen.
* Weiterführend bildet die Stoffsammlung den Ausgangspunkt für Inhalte, die in strukturierter Form für Seminare eingesetzt werden sollen.

### Wie kann ich zur Stoffsammlung beitragen? ###

* Erstell einen Fork dieses Repositories
* Füge deine Änderungen in deinem Fork hinzu.
* Erstell einen Merge Request, um deine Änderungen in dieses Repo zu überführen.